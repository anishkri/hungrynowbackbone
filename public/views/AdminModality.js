define('AdminModality',['text!../html/admin.html'],function(masterViewTemp){
	function AdminModality(){
		 this.AdminModel = Backbone.Model.extend({
			  defaults: {
			    name: 'Vipin',
			    age: '24',
			    
			  },
			  initialize: function(){
              console.log('This customer has been initialized.');
          /*    this.on('change', function(){
                  console.log('- Values for this model have changed.');
              });*/
            },
            render : function(){
           	 
            },
			});
		 this.AdminView = Backbone.View.extend({
				tagname:'div',
				template : _.template(masterViewTemp),
				events:{
					
				},
				initialize : function(){
					console.log("admin Intialized");
				},
				render : function(){
                   var s = _.template($(this.template()).html(),{});
                   this.$el.html(s);
				}
			 
		 });
	this.init();
		
	}
	AdminModality.prototype.init = function(data){
		this.adminModel = new this.AdminModel(data);
		this.adminView = new this.AdminView({ el: $("#mainContainer") },data);
			/*	provide suitable ID everytime to avoid error*/

	};
	AdminModality.prototype.renderDefault = function(data){
		this.adminView.render(this.AdminModel.attributes,function(){

		});
	};
	return AdminModality;

});