define('menuCollectionView',['menuItemView','text!../html/menuCollection.html'],function(MenuItemView,template){
	"use strict";
	var menuCollectionView = Backbone.View.extend({
		el:".simple-text",
		template : _.template(template),
		initialize : function(){
            this.listenTo(this.collection,"add",this.collectionChange);
		},
        render : function(){
            var s = _.template($(this.template()).html(),{});
            this.$el.html(s);
        },
		collectionChange : function(item){
			var menuItemView = new MenuItemView({model:item});
            this.$el.append(menuItemView.render().el);
		}
	});
	return menuCollectionView;
});