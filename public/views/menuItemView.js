define('menuItemView',['menuView','text!../html/menuItem.html','MenuCollection','CartItemModel','CartItemCollection','cartView','totalView','BillModel'],function(menuView,template,MenuCollection,CartItemModel,CartItemCollection,CartView,totalView,BillModel){
	"use strict";
	var total = 0;
	var menuItemView = Backbone.View.extend({
		template : _.template(template),
        className: "col-lg-4 col-md-4 col-sm-4 col-xs-12 single-item-div",
		events:{
			'click #addOrder':"yourCart",
            'click .item-buttons .fa-angle-up':"increaseQty",
            'click .item-buttons .fa-angle-down':"decreaseQty"
		},
		
		initialize : function(){
			this.listenTo(this.model,"change",this.render);
			
			this.billModel = new BillModel();
		    this.cartItem = new CartItemModel; 
		    this.listenTo(this.cartItem,"change",this.render);
			this.cartCollection = new CartItemCollection();
			this.totalView = new totalView({el:"#total",model:this.billModel});
			this.subTotalView = new totalView({el:"#subTotal",model:this.billModel});
			//this.totalView = new totalView({el:$("#total") ,model:this.billModel});
			 this.cart = new CartView({model: this.cartItem,collection:this.cartCollection},{scene:this});
			 this.view =	{
					 			"cart":this.cart
							}
		},
		render : function(){
		    this.$el.html($(this.template(this.model.attributes)).html());
            return this;
		},
		output:function(){
			console.log(this.model.attributes);
		},
        yourCart : function(){
        
         
			this.setDefaultPositive();
		
		
				
            
            this.cart.render();
          // this.totalView.render();
    
            //this.increaseQty();
		},
		positiveBill : function(){
			total = this.model.attributes.price + total;
			return total ;
		},
		negetiveBill : function(){
			total = total - this.model.attributes.price ;
			return total ;
		},
		refreshBill : function(){
			total = total - this.model.attributes.total_price ;
			return total ;	
		},
        increaseQty:function(){
            if(this.model.get("quantity_ordered")==this.model.get("quantity_remaining")){
                alert("cannot add more");
            }
            else{
            	this.setDefaultPositive();
            }
        },
        setDefaultPositive : function(){
   
        	this.model.set("quantity_ordered",this.model.get("quantity_ordered")+1);
            this.model.set("total_price",this.model.get("quantity_ordered")*this.model.get("price"));
            this.bill = this.positiveBill();
            this.cartItem.setItem(this.model);
            this.billModel.setItem(this.model,this.bill);
            this.$el.find(".add-order-button").hide();
            this.$el.find(".on-click-only").show();
            this.cartCollection.add(this.cartItem);
        },
        decreaseQty:function(){
            this.model.set("quantity_ordered",this.model.get("quantity_ordered")-1);
            this.model.set("total_price",this.model.get("quantity_ordered")*this.model.get("price"));
            this.cartItem.setItem(this.model);
            this.bill = this.negetiveBill();
            this.cartCollection.add(this.cartItem);
            this.billModel.setItem(this.model,this.bill);
            this.$el.find(".add-order-button").hide();
            this.$el.find(".on-click-only").show();
            if(this.model.get("quantity_ordered")==0){
                this.cartItem.deleteItem();
                this.$el.find(".on-click-only").hide();
                this.$el.find(".add-order-button").show();	
            }
        },
        removeAll:function(){
        	this.model.set("quantity_ordered",0);
        	 this.cartItem.setItem(this.model);
        	  this.bill = this.refreshBill();
              this.cartCollection.add(this.cartItem);
              this.billModel.setItem(this.model,this.bill);
            this.cartItem.deleteItem();
            this.$el.find(".on-click-only").hide();
            this.$el.find(".add-order-button").show();
        }
	});
	return menuItemView;
});