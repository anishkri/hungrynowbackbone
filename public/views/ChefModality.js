define('ChefModality',['chefForms','chefItem','text!../html/chef.html'],function(chefForms,chefItem,masterViewTemp){
	function ChefModality(){
		 this.ChefModel = Backbone.Model.extend({
			  defaults: {
			    name: 'Saurabh',
			    age: '23',
			    items:[]
			  },
			  initialize: function(){
                console.log('This model has been initialized.');
                this.on('change', function(){
                    console.log('- Values for this model have changed.');
                });
              }
         });
		 this.views ={
			"chefForms"	: new chefForms()
		 };
		 this.MasterView = Backbone.View.extend({
				tagname:'div',
				template : _.template(masterViewTemp),
				events : {
					"click .add-meal-button":'openForm',
                    "click #logOut":'logOut'
				},
				openForm : function(e){
                    e.preventDefault();
                    e.stopImmediatePropagation();
				    var form = new chefForms;
                    form.render();
                    $('.datepicker').datepicker({
                        format: 'dd/mm/yyyy',
                        todayBtn: true
                    });
				},
                logOut : function(e){
                    e.preventDefault();
                    Parse.User.logOut();
                    alert("You have been logged out");
                    this.destroy_view();
                    location.reload();
                },
                destroy_view: function() {
                    this.undelegateEvents();
                    $(this.el).removeData().unbind(); 
                    this.remove();  
                    Backbone.View.prototype.remove.call(this);
                },
				initialize:function(options){
					console.log('initialize view');
				},
				render:function(user,el){
                    var userData = {"chef_name": user.get("screen_name")};
                    var s = _.template($(this.template(userData)).html(),{});
                    this.$el.html(s);
                    if(user.get("emailVerified")==true && user.get("isChefVerified")==true)
                        this.getItemsForChef(user,".items");
                    else if(user.get("emailVerified")==true)
                        alert("You have not been verified as a chef yet");
                    else{
                        this.getItemsForChef(user,".items");
                        alert("Your email hasn't been verified yet");
                    }
				},
                getItemsForChef: function(user, el){
                    var itemsQuery = new Parse.Query(Objects.FoodItem), that=this;
                    itemsQuery.equalTo("user",user);
                    itemsQuery.include("user");
                    itemsQuery.find({
                        success:function(items){
                            items.forEach(function(item){
                                var itemDiv = new chefItem, itemToRender = {"name": item.get("name"), "objectId": item.id, "description": item.get("description"), "price": item.get("price"), "imageURL": item.get("cover_image").url()};
                                itemDiv.render(itemToRender,el);
                            });
                        },
                        error: function(result, error){
                            alert(error.code+" "+error.message);
                        }
                    });
                }
			});
		 this.init();
	}
	ChefModality.prototype.init = function(data){
		this.chefModel = new this.ChefModel(data);
		this.masterView = new this.MasterView({ el: $("#mainContainer") },data);
        return this;
			/*	provide suitable ID everytime to avoid error*/

	};
	ChefModality.prototype.renderDefault = function(data, el){
		this.masterView.render(data, el);
	};
	return ChefModality;

});