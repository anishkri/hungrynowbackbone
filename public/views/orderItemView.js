define('orderItemView',['text!../html/orderItem.html'],function(template){
	"use strict";
    var orderItemView = Backbone.View.extend({
        tagname:'div',
        el:"#orderView",
        template : _.template(template),
        events:{
            "click .to-open": "openDiv",
            "click .rating-star": "setRating",
            "click .btn-submit-feedback": "saveFeedback",
            "click .btn-submit-issue": "saveIssue"
        },
        initialize : function(el){
            this.listenTo(this.model,"change",this.render);
            this.listenTo(this.model,"destroy",this.deleteView);
        },
        render : function(menuCollection){
            this.$el.html($(this.template(this.model.attributes)).html());
            return this; 
        },
        openDiv : function(ev){
            ev.stopImmediatePropagation();
            $(ev.currentTarget).find(".fa-angle-right").toggleClass("rotated");
            $(ev.currentTarget).parent().find(".to-hide").slideToggle();
            setTimeout(function(){window.scrollBy(0,300)},300);
        },
        saveFeedback : function(ev){
            var ratingDivs = $(ev.currentTarget).closest(".order-feedback-div").find(".rating"), ratings = {};
            for(var i=0;i<ratingDivs.length;i++){
                ratings[$(ratingDivs[i]).attr("data-type")] = $(ratingDivs[i]).attr("data-rating");
            }
            var feedback = new Objects.Feedback(), comments = $(ev.currentTarget).closest(".order-feedback-div").find(".feedback").val();
            feedback.set("ratings",ratings);
            feedback.set("comments",comments);
            var order = new Objects.Order();
            order.id = this.model.get("objectId");
            feedback.set("order",order);
            feedback.save(null,{
                success:function(feedback){
                    $(ev.currentTarget).attr("disabled","disabled");
                    alert("feedback saved successfully");
                },
                error:function(error){
                    alert("couldn't save due to some error");
                }
            });
            console.log(ratings);
        },
        saveIssue : function(ev){
            var issue = new Objects.Issue(), comments = $(ev.currentTarget).closest(".order-issue-div").find(".issue").val();
            issue.set("comments",comments);
            var order = new Objects.Order();
            order.id = this.model.get("objectId");
            issue.set("order",order);
            issue.save(null,{
                success:function(issue){
                    $(ev.currentTarget).attr("disabled","disabled");
                    alert("issue saved successfully");
                },
                error:function(error){
                    alert("couldn't save due to some error");
                }
            });
        },
        setRating : function(ev){
            var star = $(ev.currentTarget), clickedStar = star, rating=0;
            while(star.prop("tagName")=="I"){
                star.removeClass("fa-star-o").addClass("fa-star");
                star = star.prev();
                rating++;
            }
            star = clickedStar;
            while(star.next().prop("tagName")=="I"){
                star = star.next();
                star.removeClass("fa-star").addClass("fa-star-o");
            }
            clickedStar.closest(".rating").attr("data-rating",rating);
        }
	});
	return orderItemView;
});