define('chefItem',['text!../html/chefItem.html','chefForms'],function(item,chefForms){
    "use strict";
	var ItemView = Backbone.View.extend({
		tagname:'div',
		template : _.template(item),
        el: '.items',
		events : {
            "click .fa-pencil" : "editItem",
            "click .fa-times-circle" : "deleteItem"
		},
		initialize:function(options){
			console.log('initialize view');
		},
		render:function(item,el){
			var s = $(this.template(item)).html();
			$(el).append(s);
		},
        editItem:function(ev){
            ev.stopImmediatePropagation();
            var itemId = $(ev.currentTarget).closest(".item-div").attr("data-itemId"), query = new Parse.Query(Objects.FoodItem);
            query.equalTo("objectId",itemId);
            alert("Item to be edited "+itemId);
            query.first({
                success:function(item){
                    var editForm = new chefForms();
                    editForm.renderWithData(item);
                    $('.datepicker').datepicker({
                        format: 'dd/mm/yyyy',
                        todayBtn: true
                    });
                },
                error:function(error){
                    alert("Error: "+error.code+" "+error.message);   
                }
            });
        },
        deleteItem:function(ev){
            ev.stopImmediatePropagation();
            var itemId = $(ev.currentTarget).closest(".item-div").attr("data-itemId"), query = new Parse.Query(Objects.FoodItem);
            query.equalTo("objectId",itemId);
            alert("Item to be deleted "+itemId);
            query.first({
                success:function(item){
                    item.destroy({
                        success:function(item){
                            alert("item successfully deleted");
                        },
                        error:function(error){
                            alert("Error: "+error.code+" "+error.message);  
                        }
                    });
                },
                error:function(error){
                    alert("Error: "+error.code+" "+error.message);   
                }
            });
        }
	});
	return ItemView;
});