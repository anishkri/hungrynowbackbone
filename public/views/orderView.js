define('orderView',['OrderItemModel','OrderItemCollection','text!../html/order.html','orderItemView'],function(OrderItemModel,OrderItemCollection,template,orderItemView){
	var orderView = Backbone.View.extend({
		   tagName: 'div',
	       template: _.template(template),
	       el:"#foodOrder",
	       events: {
	            'click #addOrder': "output"
	       },
	       initialize: function(options) {
	           this.options = options;
	           this.listenTo(this.collection, "add", this.update);
	       },
	       render: function(item) {
	    	   var s = _.template($(this.template(this.model.attributes)).html(),{});
	    	   this.$el.append(s);
	       },
	       update : function (item){
	    	  var eachOrder =  new orderItemView({model: item,collection:this.collection})
	    	  this.$el.append(eachOrder.render().el);
	       }
	    
	});
	return orderView;
});