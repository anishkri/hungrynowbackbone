define('chefForms',['text!../html/form.html'],function(form){

	"use strict";
	var FormView = Backbone.View.extend({
		tagname:'div',
		template : _.template(form),
        el: '#overlayContainer',
		events : {
            "submit #add_item_form": "addFoodItem"
		},
		initialize:function(options){
			console.log('initialize view');
		},
		render:function(){
			var data={};
            data.objectId = "";
            data.name = "";
            data.description = "";
            data.price = "";
            data.quantity = "";
            data.available_on = "";
            data.available_for = "";
            data.imageURL = "";
            var s = $(this.template(data)).html();
			$('#overlayContainer').html(s);
            $("#order-modal").modal("show");
            this.currentUser = Parse.User.current();
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true
            });
		},
        renderWithData:function(data){
            data.objectId = data.id;
            data.name = data.get("name");
            data.description = data.get("description");
            data.price = data.get("price");
            data.quantity = data.get("quantity");
            data.available_on = data.get("available_on");
            data.available_on = ("0"+data.available_on.getUTCDate()).slice(-2)+"/"+("0"+(data.available_on.getUTCMonth()+1)).slice(-2)+"/"+data.available_on.getUTCFullYear();
            data.available_for = data.get("available_for");
            data.imageURL = data.get("imageURL");
            var s = $(this.template(data)).html();
			$('#overlayContainer').html(s);
            $("#order-modal").modal("show");
            this.currentUser = Parse.User.current();
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true
            });
        },
        addFoodItem: function(ev){
            ev.preventDefault();
            ev.stopImmediatePropagation();
            this.currentUser = Parse.User.current();
            var that=this;
            var button = $('.submit_button'), itemId = $("#add_item_form").attr("data-itemid");
            if(itemId.length==0){
                button.prop('disabled', true).text("Uploading Images");
                var fileUploadControl = $("#food_images")[0];
                var isError = false;
                var file;
                if (fileUploadControl.files.length > 0) {
                    file = fileUploadControl.files[0];
                    var name = "cover";
                    var parseFile = new Parse.File(name, file);
                    parseFile.save().then(function() {
                        button.text("Uploading Food Metadata");
                        var FoodItem = Parse.Object.extend("FoodItem");
                        var foodItem = new FoodItem();
                        var foodACL = new Parse.ACL(that.currentUser);
                        foodACL.setPublicReadAccess(true);
                        foodItem.set("cover_image", parseFile);
                        foodItem.set("name", $('#name').val());
                        foodItem.set("description", $('#description').val());
                        foodItem.set("available_on", new Date($('.datepicker').datepicker('getUTCDate')));
                        foodItem.set("available_for", $('#available_for').val());
                        foodItem.set("quantity", Number($('#quantity').val()));
                        foodItem.set("quantity_remaining", Number($('#quantity').val()));
                        foodItem.setACL(foodACL);
                        foodItem.set("user", that.currentUser);
                        foodItem.set('price', $('#price').val());
                        foodItem.set('denomination', "RUPEE");
                        foodItem.save(null, {
                            success: function(foodItem) {
                                alert("Food Item Has Been Saved");
                                button.prop('disabled', false);
                                button.text("Submit");
                                $("#order-modal").modal("hide");
                            },
                            error: function(foodItem, error) {
                                alert ("error");
                                button.prop('disabled', false);
                                button.text("Submit");
                            }
                        });
                    }, function(error) {
                        button.prop('disabled', false);
                        button.text("Submit");
                        alert ("error");
                    });
                } else {
                    isError = true;
                    button.prop('disabled', false);
                    button.text("Submit");
                }
            }
            else{
                button.prop('disabled', true).text("Updating Item");
                var FoodItem = Parse.Object.extend("FoodItem");
                var itemQuery = new Parse.Query(FoodItem);
                itemQuery.equalTo("objectId",itemId);
                itemQuery.first({
                    success:function(foodItem){
                        foodItem.set("name", $('#name').val());
                        foodItem.set("description", $('#description').val());
                        foodItem.set("available_on", new Date($('.datepicker').datepicker('getUTCDate')));
                        foodItem.set("available_for", $('#available_for').val());
                        foodItem.set("quantity", Number($('#quantity').val()));
                        foodItem.set("quantity_remaining", Number($('#quantity').val()));
                        foodItem.set('price', $('#price').val());
                        foodItem.set('denomination', "RUPEE");
                        foodItem.save(null,{
                            success:function(item){
                                button.prop('disabled', false);
                                button.text("Submit");
                                $("#order-modal").modal("hide");
                                alert("successfully updated item");
                            },
                            error: function(foodItem, error) {
                                alert ("error");
                                button.prop('disabled', false);
                                button.text("Submit");
                            }
                        });
                    },
                    error: function(foodItem, error) {
                        alert ("error");
                        button.prop('disabled', false);
                        button.text("Submit");
                    }
                });
            }
        }
	});
	return FormView;
});