define('CustomerModality',['text!../html/customer.html'],function(masterViewTemp){
	function CustomerModality(){
		 this.CustomerModel = Backbone.Model.extend({
			  defaults: {
			    name: 'Vipin',
			    age: '24',
			    
			  },
			  initialize: function(){
               console.log('This customer has been initialized.');
           /*    this.on('change', function(){
                   console.log('- Values for this model have changed.');
               });*/
             },
             render : function(){
            	 
             },
			});
		 this.CustomerView = Backbone.View.extend({
				tagname:'div',
				template : _.template(masterViewTemp),
				events:{
					
				},
				initialize : function(){
					console.log("Customer Intialized");
				},
				render : function(){
                    var s = _.template($(this.template()).html(),{});
                    this.$el.html(s);
				}
			 
		 });
	this.init();
	}
	CustomerModality.prototype.init = function(data){
		this.chefModel = new this.CustomerModel(data);
		this.masterView = new this.CustomerView({ el: $("#mainContainer") },data);
			/*	provide suitable ID everytime to avoid error*/

	};
	CustomerModality.prototype.renderDefault = function(data){
		this.masterView.render(this.CustomerModel.attributes,function(){

		});
	};
	return CustomerModality;
});