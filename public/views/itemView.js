define('itemView',['text!../html/chefItem.html','chefForms'],function(template,chefForms){
    "use strict";
    var ItemView = Backbone.View.extend({
        template : _.template(template),
        options:null,
        initialize:function(options){
            this.options = options;
            this.listenTo(this.model,"change",this.render);
            this.listenTo(this.model,"destroy",this.destroy);
        },
        events : {
            "click .fa-pencil" : "editItem",
            "click .fa-times-circle" : "deleteItem"
        },
        render:function(){
            this.$el.html($(this.template(this.model.attributes)).html());
            return this;
        },
        destroy : function(){
            this.$el.remove();
        },
        editItem:function(){
            console.log(this.model);
            var editForm = new chefForms();
            editForm.renderWithData(model);
        },
        deleteItem:function(){
            var itemId = this.model.get("objectId"), query = new Parse.Query(Objects.FoodItem),that = this;
            query.equalTo("objectId",itemId);
            query.first({
                success:function(item){
                    item.destroy({
                        success:function(item){
                            alert("item successfully deleted");
                            that.model.deleteItem();
                        },
                        error:function(error){
                            alert("Error: "+error.code+" "+error.message);
                        }
                    });
                },
                error:function(error){
                    alert("Error: "+error.code+" "+error.message);
                }
            });
        }
    });
    return ItemView;
});