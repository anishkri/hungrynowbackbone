define('cartItemView',['text!../html/cartItem.html'],function(template){
	"use strict";
    var cartItemView = Backbone.View.extend({
        tagname:'div',
        template : _.template(template),
        events:{
            "click .fa-times": "deleteItem"
        },
        initialize : function(el,scene){
        	this.scene = scene;
            this.listenTo(this.model,"change",this.render);
            this.listenTo(this.model,"destroy",this.deleteView);
        },
        render : function(menuCollection){
            this.$el.html($(this.template(this.model.attributes)).html());
            return this; 
        },
        deleteItem: function(){
            this.model.deleteItem();
         //   this.scene.scene.scene.billModel.setItem(this.model,this.scene.scene.scene.negetiveBill())
            this.scene.scene.scene.removeAll()
      /*  	var menuItemView = new MenuItemView({model:item});
            this.$el.append(menuItemView.render().el);*/
        },
        deleteView : function(){
            this.$el.detach();
        }
	});
	return cartItemView;
});