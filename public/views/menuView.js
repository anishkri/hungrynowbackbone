define('menuView',['menuCollectionView','text!../html/menu.html'],function(MenuCollectionView,template){
	"use strict";
	var menuView = Backbone.View.extend({
		el:"#mainContainer",
		template : _.template(template),
        events: {
            "click #show-address":"showAddress",
            "focus .address-details":"moveLabels",
            "blur .address-details":"moveLabels",
            "click .address-line-label":"focusText",
            "click .place-order-btn":"placeOrder",
            "keypress .phone-number":"restrictToNumber",
            "keyup .phone-number":"restrictToNumber",
            "change .phone-number":"restrictToNumber"
        },
		initialize : function(){
		},
        render : function(menuCollection){
            var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'], months = ['January','February','March','April','May','June','July','August','September','October','November','December'], now = new Date();
            var date = {day: days[now.getDay()], date: now.getDate(), month: months[now.getMonth()], year: now.getFullYear()};
            var s = _.template($(this.template(date)).html(),{});
            this.$el.html(s);
            this.menuCollectionView = new MenuCollectionView({collection: menuCollection});
            this.menuCollectionView.render();
        },
        showAddress : function(ev){
            $(ev.currentTarget).toggleClass("rotated");
            this.$el.find(".to-show-on-click").slideToggle();
        },
        moveLabels : function(ev){
            $(ev.currentTarget).next().toggleClass("activated");
            if($(ev.currentTarget).val().length != 0)
                $(ev.currentTarget).next().addClass("activated");
        },
        focusText : function(ev){
            $(ev.currentTarget).prev().focus();
        },
        restrictToNumber : function(e){
        	if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        	if($(e.currentTarget).val().length==10){
        		return false;
        	}
        },
        placeOrder : function(ev){
        	var orders = this.menuCollectionView.collection.models.filter(function(model){return model.get("quantity_ordered")>0}), orderObject={};
        	orders.forEach(function(order){
        		orderObject[order.get("objectId")] = order.get("quantity_ordered");
        	});
        	var itemCount = Object.keys(orderObject).length;
        	if(itemCount==0)
        		alert("Order some food");
        	
        	var phNo = "91" + $(".phone-number").val(), emailId = $(".e-mail").val(), name = $(".name").val(), address = $(".full-address").val(), addressType = $(".address-type:checked").val(), addressId, geoPosition;
            if(phNo.length!=12){
                $(".alert-danger h4").text("Please enter your 10 digit phone number");
                $(".alert-danger").show();
                alert("Please enter your 10 digit phone number");
                $(".close").attr("disabled",false);
                return false;
            }
            if(emailId.length==0){
                $(".alert-danger h4").text("Please enter your email id");
                $(".alert-danger").show();
                alert("Please enter your email id");
                $(".close").attr("disabled",false);
                return false;   
            }
            if(address.length==0){
                $(".alert-danger h4").text("Please enter your email id");
                $(".alert-danger").show();
                alert("Please enter your address");
                $(".close").attr("disabled",false);
                return false;   
            }
            if (navigator.geolocation) {
               navigator.geolocation.getCurrentPosition(showPosition);
            }
            else { 
                alert("Geolocation is not supported by this browser.");
            }
            function showPosition(position){
         	   geoPosition = position;
         	   saveAddress();
            }
            function saveAddress(){
            	var addressObject = new Objects.Address(), area = new Objects.AreaDictionary();
                area.id = "8LxWP8gYdx";
                addressObject.set("address_type",addressType)
                addressObject.set("address_line_1",address);
                addressObject.set("area", area);
            	addressObject.set("location", new Parse.GeoPoint({latitude:geoPosition.coords.latitude,longitude:geoPosition.coords.longitude}));
                addressObject.set("user_name", emailId);
                addressObject.save(null,{
                    success:function(address){
                        addressId = address.id;
                        console.log(addressId);
                        confirmOrder(orderObject, phNo, emailId, addressId);
                    },
                    error:function(address, error){
                        alert("Couldn't save address due to some error.");
                    }
                });
            }
            function confirmOrder(orderObject, phNo, emailId, addressId){
                var orderDetails = {
                    orderDetails: JSON.stringify(orderObject),
                    phNo: phNo,
                    emailId: emailId,
                    address: addressId
                };
                
                $.post('https://hungrynowdev.parseapp.com/orderNow', orderDetails)
                    .done(function(data) {     
                        alert("Your order id is "+data.objectId);
                    })
                    .fail(function(data) {
                        data = data.responseJSON;
                        var alertText = "Only ",count=0;
                        for(key in data){
                            if(data[key].quantity_remaining==0){
                                var imageUrl = $(".menu-item-caption[data-itemId='"+key+"']").prev().css("background-image");
                                $(".menu-item-caption[data-itemId='"+key+"']").prev().css("background-image",imageUrl+",url('lib/images/sold-out.png')");
                                $("ul[data-itemId='"+key+"']").closest(".item").find(".confirm-item-remove-div").find(".yes").click();
                            }
                            $(".item-quantity").find(".dropdown-menu[data-itemId='"+key+"']").prev().html(data[key].quantity_remaining+"<i class='fa fa-caret-down right'></i>");
                            $(".item-quantity").find(".dropdown-menu[data-itemId='"+key+"'] a:contains('"+data[key].quantity_remaining+"')").click();
                            for(var j=0;j<$(".item-quantity").find(".dropdown-menu[data-itemId='"+key+"'] li").length;j++)
                                if(parseInt($($(".item-quantity").find(".dropdown-menu[data-itemId='"+key+"'] li")[j]).text())>data[key].quantity_remaining)
                                    $($(".item-quantity").find(".dropdown-menu[data-itemId='"+key+"'] li")[j]).addClass("to-hide");
                            $(".menu-item-caption[data-itemId='"+key+"']").find(".item-left-count").attr({"data-item-left-count":0,"data-item-left-original-count":data[key].quantity_remaining});
                            $(".menu-item-caption[data-itemId='"+key+"']").find(".item-left-count span").text(0+ " meals available");
                            $(".menu-item-caption[data-itemId='"+key+"']").find(".item-ordered-count span").text(data[key].quantity_remaining+ " meals added");
                            $(".menu-item-caption[data-itemId='"+key+"']").find(".item-ordered-count").attr("data-item-ordered-count",data[key].quantity_remaining);
                            orderObject[key]=data[key].quantity_remaining;
                            count++;
                            if(count==1)
                                alertText = alertText+data[key].quantity_remaining+" of "+data[key].name+" is available";
                            else if(count>1){
                                alertText = alertText.replace("is available","");
                                var i = alertText.indexOf("and");
                                if(i!=-1)
                                    alertText = alertText.replace("and",",");
                                alertText = alertText+" and "+data[key].quantity_remaining+" of "+data[key].name+" is available";
                            }
                        }
                        //orderDone = 0;
                        alertText = alertText+". Please review your order and confirm again.";
                        alert(alertText);
                        $(".alert-danger h4").text(alertText);
                        $(".alert-danger").show();
                    }).always(function() {
                        location.reload(function(){
                        
                        });
                    });
            }
        }
	});
	return menuView;
});