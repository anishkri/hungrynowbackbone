define('totalView',['text!../html/total.html'],function(temp){
	"use strict"
	var totalView = Backbone.View.extend({
		   tagName: 'div',
	       template: _.template(temp),
	   
	       events: {
	            
	       },
	       initialize: function(options) {
	    	   this.listenTo(this.model,"change",this.render);
	       },
	       render: function(item) {
	    	   var s = _.template($(this.template(this.model.attributes)).html(),{});
	    	   this.$el.html(s);
	       },
	       update : function (item){
	    	   var s = _.template($(this.template(this.model.attributes)).html(),{});
	    	   this.$el.html(s);
	       }
	    
	});
	return totalView;
	
})