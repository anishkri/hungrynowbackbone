define('itemCollectionView',['itemView','chefForms'],function(ItemView,chefForms){
    "use strict";
    var ItemCollectionView= Backbone.View.extend({
        el: ".items",
        initialize:function(){
            this.listenTo(this.collection,"add",this.render);
        },
        render: function(item){
            var itemView= new ItemView({model:item});
		    this.$el.append(itemView.render().el);
        }
    });
    return ItemCollectionView;
});