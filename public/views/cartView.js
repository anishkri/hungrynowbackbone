define('cartView',['CartItemModel','CartItemCollection','text!../html/cart.html','cartItemView'],function(CartItemModel,CartItemCollection,template,cartItemView){
	var cartView = Backbone.View.extend({
		   tagName: 'div',
	       template: _.template(template),
	       el:"#foodOrder",
	       events: {
	            'click #addOrder': "output"
	       },
	       initialize: function(options,scene) {
	           this.options = options;
	           this.listenTo(this.collection, "add", this.update);
	           this.eachOrder =  new cartItemView({model: this.model,collection:this.collection},{scene:scene});
	           this.view={
	        		   "item":this.eachOrder
	           }
	       },
	       render: function(item) {
	    	  
	    	   var s = _.template($(this.template(this.model.attributes)).html(),{});
	    	   this.$el.append(s);
	       },
	       update : function (item){
	    	 
	    	
	    	  this.$el.append(this.eachOrder.render().el);
	       }
	    
	});
	return cartView;
});