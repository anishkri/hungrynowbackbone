define('chefLogin',['text!../html/chefLogin.html','ChefModality'],function(loginView,ChefModality){
    "use strict";
	var LoginView = Backbone.View.extend({
		tagname:'div',
        el: '#mainContainer',
		template : _.template(loginView),
		events : {
            "click .fbSigninButton": "loginWithFB",
            "submit #signin_form": "login"
		},
		initialize:function(options){
			console.log('initialize view');
		},
		render:function(){
			var s = $(this.template()).html();
			$(this.el).html(s);
		},
        login:function(e){
            e.preventDefault();
            var inputs = $(e.currentTarget).serializeArray(), formObj = new Object(), that=this;
            $.each(inputs, function(i,input) {
                formObj[input.name] = input.value;
            });
            Parse.User.logIn(formObj.username, formObj.password, {
                success:function(result) {
                    var chefPage = new ChefModality;
                    chefPage.renderDefault(result, that.el);
                },
                error:function(res,error) {
                    that.render();
                }
            });
        },
        loginWithFB:function(){
            Parse.FacebookUtils.logIn("email", {
              success: function(user) {
                if (!user.existed()) {
                    alert("User signed up and logged in through Facebook! "+user.email+" "+user.first_name);
                } else {
                    alert("User logged in through Facebook! "+user.email+" "+user.first_name);
                }
              },
              error: function(user, error) {
                    alert("User cancelled the Facebook login or did not fully authorize.");
              }
            });
        }
	});
	return LoginView;
});