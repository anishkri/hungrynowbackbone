define('registerView',['text!../html/userRegister.html','ChefModality'],function(registerView,ChefModality){
    "use strict";
	var registerView = Backbone.View.extend({
		tagname:'div',
        el: '#mainContainer',
		template : _.template(registerView),
		events : {
            "click .fbSignUpButton": "signupWithFB",
            "submit #signup_form": "signup"
		},
		initialize:function(options){
			console.log('initialize view');
		},
		render:function(){
			var s = $(this.template()).html();
			$(this.el).html(s);
		},
        signup:function(e){
            e.preventDefault();
            var inputs = $(e.currentTarget).serializeArray(), formObj = new Object(), that=this;
            $.each(inputs, function(i,input) {
                formObj[input.name] = input.value;
            });
            var user = new Parse.User();
            user.set("username", formObj.username);
            user.set("password", formObj.password);
            user.set("email", formObj.username);
            user.set("phone", formObj.phone);
            user.set("type", formObj.role);
            user.set("isChefVerified", false);
            user.set("screen_name", formObj.name);
            user.signUp(null, {
                success:function(result) {
                    alert("User successfully registered");
                    var chefPage = new ChefModality;
                    chefPage.renderDefault(result, that.el);
                },
                error:function(res,error) {
                    this.render();
                }
            });
        },
        signupWithFB:function(){
            Parse.FacebookUtils.logIn("email,", {
              success: function(user) {
                if (!user.existed()) {
                    FB.api('/me',function(me){
                        user.set("isChefVerified",false);
                        user.set("email",me.email);
                        user.set("screen_name",me.first_name);
                        user.set("type","customer");
                        user.save();
                    });
                    alert("User signed up and logged in through Facebook!");
                } else {
                    alert("User logged in through Facebook!");
                }
              },
              error: function(user, error) {
                    alert("User cancelled the Facebook login or did not fully authorize.");
              }
            });
        }
	});
	return registerView;
});