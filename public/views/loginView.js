define('loginView',['text!../html/login.html','ChefModality','CustomerModality'],function(loginTemplate,ChefModality,CustomerModality){
    "use strict";
	var LoginView = Backbone.View.extend({
		tagname:'div',
        el: '#mainContainer',
		template : _.template(loginTemplate),
		events : {
            "click .fbSigninButton": "loginWithFB",
            "submit #signin_form": "login"
		},
		initialize:function(options){
			console.log('initialize view');
		},
		render:function(){
			var s = $(this.template()).html();
			$(this.el).html(s);
		},
        login:function(e){
            e.preventDefault();
            var inputs = $(e.currentTarget).serializeArray(), formObj = new Object(), that=this;
            $.each(inputs, function(i,input) {
                formObj[input.name] = input.value;
            });
            Parse.User.logIn(formObj.username, formObj.password, {
                success:function(user) {
                    if(user.get("type")=="chef"){
                        var chefPage = new ChefModality;
                        chefPage.renderDefault(user, that.el);
                    }
                    else if(user.get("type")=="customer"){
                        var customerPage = new CustomerModality;
                        customerPage.renderDefault(user, that.el);
                    }
                    else if(user.get("type")=="admin"){
                        var adminPage = new AdminModality;
                        adminPage.renderDefault(user, that.el);
                    }
                },
                error:function(res,error) {
                    that.render();
                }
            });
        },
        loginWithFB:function(){
            var that=this;
            Parse.FacebookUtils.logIn("email", {
              success: function(user) {
                if (!user.existed()) {
                    FB.api('/me',function(me){
                        user.set("isChefVerified",false);
                        user.set("email",me.email);
                        user.set("screen_name",me.first_name);
                        user.set("type","customer");
                        user.save();
                    });
                    alert("User signed up and logged in through Facebook!");
                }
                if(user.get("type")=="chef"){
                    var chefPage = new ChefModality;
                    chefPage.renderDefault(user, that.el);
                }
                else if(user.get("type")=="customer"){
                    var customerPage = new CustomerModality;
                    customerPage.renderDefault(user, that.el);
                }
                else if(user.get("type")=="admin"){
                    var adminPage = new AdminModality;
                    adminPage.renderDefault(user, that.el);
                }
              },
              error: function(user, error) {
                    alert("User cancelled the Facebook login or did not fully authorize.");
              }
            });
        }
	});
	return LoginView;
});