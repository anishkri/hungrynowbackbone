module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    /*aws: grunt.file.readJSON('aws.json'),*/

    copy : {
     createBuild : {
         files : [{
             cwd : '<%=pkg.buildTarget%>/',
             expand : true,
             src : [ '../views/**/*.js', '../modalities/**/*.js', '../models/**/*.js', '../collections/**/*.js' ],
             dest : '<%=pkg.buildTarget%>/*'
         }]
     },
  	copycss:{
  		 files : [{
             cwd : '<%=pkg.buildTarget%>/',
             expand : true,
             src : [ '../css/*.css' ],
             dest : '<%=pkg.buildTarget%>/*'
         }]
  	},
  	copyhtml:{
 		 files : [{
            cwd : '<%=pkg.buildTarget%>/',
            expand : true,
            src : [ '../*.html' ],
            dest : '<%=pkg.deployPath%><%=pkg.name%>/*.html'
        }]
 	},
 	copyassets:{
		 files : [{
           cwd : '<%=pkg.buildTarget%>/',
           expand : true,
           src : [ '../assets/**' ],
           dest : '<%=pkg.deployPath%><%=pkg.name%>/*.'
       }]
	},
	  copyLib : {
	         files : [{
	             cwd : '<%=pkg.buildTarget%>/',
	             expand : true,
	             src : [ '../lib/**' ],
	             dest : '<%=pkg.deployPath%><%=pkg.name%>/*'
	         }]
	     },
	     copyHtmlFiles : {
	         files : [{
	             cwd : '<%=pkg.buildTarget%>/',
	             expand : true,
	             src : [ '../html/*.html' ],
	             dest : '<%=pkg.deployPath%><%=pkg.name%>/*'
	         }]
	     },
  /*,*/
/*     copyHtml : {
         files : [{
             cwd : '<%=pkg.buildTarget%>',
             expand : true,
             src : [ '**' ],
             dest : '<%=pkg.deployPath%>/<%=pkg.version%>/'
         }]
     },

     copyLib : {
         files : [{
             cwd : '../<%=pkg.version%>/',
             expand : true,
             src : [ 'lib/**' ],
             dest : '<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>/'
         }]
     },

     copyLibDev : {
      files : [{
          cwd : '../<%=pkg.version%>/lib-dev/',
          expand : true,
          src : [ '**' ],
          dest : '<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>/lib'
      }]
     },

     copyAssets : {
      files : [{
          cwd : '../<%=pkg.version%>/',
          expand : true,
          src : [ 'assets/**' ],
          dest : '<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>'
      }]
    },*/
    },
//  our concat options
    concat : {
        options : {
            separator : ';' // separates scripts
        },

        dist : {
            src : [ '../js/main.js','../js/app.js','<%=pkg.buildTarget%>/**/*.js'], // Using mini match for your
            // scripts to concatenate
            dest : '<%=pkg.deployPath%><%=pkg.name%>/js/<%=pkg.name%>.js' // where to output the script
        }
    },

//  our uglify options
    uglify : {
        js : {
            files : [{
                expand : true,
                src : [ '<%=pkg.deployPath%><%=pkg.name%>/js/<%=pkg.name%>.js' ],
                dest : '../<%=pkg.name%>.js'
            }]
        }
    },

    clean : {
     options : {
         force : true
     },
     cleanDeployed : [ '<%=pkg.buildTarget%>' ],
     cleanDist : [ '<%=pkg.deployPath%>' ],
     cleanDoc:['../doc']
    },

    //  our JSHint options
    jshint : {
        all : [ '<%=pkg.buildTarget%>/**/*.js' ]
    // files to lint
    },

    cssmin : {
     minify : {
         files : [ {
             expand : true,
             src : [ '<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>/css/<%=pkg.name%>.css' ],
             dest : '../<%=pkg.name%>.css'
         }]
     }
    },

    concat_css: {
        options: {
          // Task-specific options go here.
        },
        all: {
          src: ["<%=pkg.buildTarget%>/**/*.css"],
          dest: '<%=pkg.deployPath%><%=pkg.name%>/css/<%=pkg.name%>.css'
        }
      },

/*   jsdoc : {
    dist : {
        src: [ '<%=pkg.buildTarget%>/widgets/Slider.js' ,
               '<%=pkg.buildTarget%>/widgets/Graph.js',
               '<%=pkg.buildTarget%>/scene/Scene.js' ,
               '<%=pkg.buildTarget%>/scene/SceneController.js' ,
               '<%=pkg.buildTarget%>/events/EventHandler.js' ,
               '<%=pkg.buildTarget%>/animations/Animation.js' ,
               '<%=pkg.buildTarget%>/animations/AnimTransform.js' ,
               '<%=pkg.buildTarget%>/animations/easing.js' ,
               '<%=pkg.buildTarget%>/utils/utility.js' ,
               '<%=pkg.buildTarget%>/utils/ajax.js',
               '<%=pkg.buildTarget%>/widgets/ProgressBar.js' ,
               '<%=pkg.buildTarget%>/widgets/CirclePointer.js' ,
               '<%=pkg.buildTarget%>/widgets/Toggle.js' ,
               '<%=pkg.buildTarget%>/utils/PathAnimate.js',
               '<%=pkg.buildTarget%>/utils/StaticObject.js',
               '<%=pkg.buildTarget%>/views/View.js',
               '<%=pkg.buildTarget%>/views/CommonView.js',
               '<%=pkg.buildTarget%>/views/ViewController.js',
               '<%=pkg.buildTarget%>/math/Matrix.js',
               '<%=pkg.buildTarget%>/simObject/SimObject.js',
               '<%=pkg.buildTarget%>/simObject/SoModel.js'
               ],
        dest:'../doc'
    }
   },*/

/*   s3: {
    options: {
      accessKeyId: "<%= aws.accessKeyId %>",
      secretAccessKey: "<%= aws.secretAccessKey %>",
      bucket: "<%= aws.bucket %>",
      access:"public-read-write",
      cache:"<%aws.cache %>",
      gzip:"<%aws.gzip%>"
    },
    build: {
      cwd: "<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>",
      src: "**",
      dest:"<%= aws.uploadPathDev %><%=pkg.name%>/<%=pkg.version%>/"
    },
    move: {
     cwd: "<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>",
     src: "**",
     dest:"<%= aws.uploadPathProd %><%=pkg.name%>/<%=pkg.version%>/"
    },
    moveActiveCode: {
        cwd: "<%=pkg.deployPath%><%=pkg.name%>/<%=pkg.version%>",
        src: "**",
        dest:"<%= aws.uploadPathProd %><%=pkg.name%>/"
    }
   }*/
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-concat-css');
 // grunt.loadNpmTasks('grunt-jsdoc');
 // grunt.loadNpmTasks('grunt-aws');

  // Default task(s).
  grunt.registerTask('dev', [ 'clean:cleanDeployed',
                               'clean:cleanDist',
                               'copy:createBuild',
                              'copy:copycss',
                              'copy:copyhtml',
                              'copy:copyassets',
                              'copy:copyLib',
                              'copy:copyHtmlFiles',
                              'concat',
                              'concat_css',
                              'cssmin'



                    ]);

  grunt.registerTask('jsdocs',['clean:cleanDoc']);

  grunt.registerTask('prod', [ 'clean:cleanDeployed',
                               'clean:cleanDist',
                               'copy:createBuild',
                               'concat',
                               'uglify',
                               'concat_css',
                               'cssmin',
                               'copy:copyLib',
                               'copy:copyAssets'
                    ],function(){
	  console.log()
  });

  /*grunt.registerTask('upload-dev',[
                                   's3:build'
                                   ]);

  // Upload allspark to a particular version folder
  grunt.registerTask('upload-version',[
                               's3:move'
                               ]);


  // Upload allspark to both the version folder and default folder
  grunt.registerTask('upload',[
                                       's3:move',
                                       's3:moveActiveCode'
                                       ]);
*/

};