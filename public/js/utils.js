define('utils',[],function(){
	 template = {

		    // Hash of preloaded templates for the app
		    templates:{},

		    // Recursively pre-load all the templates for the app.
		    // This implementation should be changed in a production environment. All the template files should be
		    // concatenated in a single file.
		    loadTemplates:function (names, callback) {

		        var that = this;

		        var loadTemplate = function (index) {
		            var name = names[index];
		            console.log('Loading template: ' + name);
		            $.get('html/' + name + '.html', function (data) {
		                that.templates[name] = data;
		                index++;
		                if (index < names.length) {
		                    loadTemplate(index);
		                } else {
		                    callback(data);
		                }
		            });
		        }

		        loadTemplate(0);
		    },

		    // Get template by name from hash of preloaded templates
		    get:function (name) {
		        return this.templates[name];
		    }

		};
	 
	 TemplateManager = {
			  templates: {},
			 
			  get: function(id, callback){
			    var template = this.templates[id];
			 
			    if (template) {
			      callback(template);
			 
			    } else {
			 
			      var that = this;
			      $.get("/templates/" + id + ".html", function(template){
			        var $tmpl = $(template);
			        that.templates[id] = $tmpl;
			        callback($tmpl);
			      });
			 
			    }
			 
			  }
			 
			}
});