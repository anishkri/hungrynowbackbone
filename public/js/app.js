define('app',['AdminModality','ChefModality','CustomerModality','LandingPage','registerView','loginView'],function(AdminModality,ChefModality,CustomerModality,LandingPage,registerView,loginView){
	 Objects = {
			    Order : Parse.Object.extend("Order"),
			    Address : Parse.Object.extend("Address"),
			    FoodItem : Parse.Object.extend("FoodItem"),
			    AreaDictionary : Parse.Object.extend("AreaDictionary"),
			    Feedback : Parse.Object.extend("Feedback"),
                Issue : Parse.Object.extend("Issue")
			};


	var landingPage = new LandingPage();
	//var admin = new AdminModality();
 		//landingPage.renderDefault();



 	var ApplicationRouter = Backbone.Router.extend({
 		 routes: {


             "home": "home",
             'register':'registration',
             'login':"login"
            /* "about": "about",
             "contact": "contact"*/
         },
         home : function(){
        	 landingPage.renderDefault();
         },
         registration : function(){
        		   var view = new registerView({ el: $("#mainContainer") });
                   view.render();
         },
         login : function(){
        		   var view = new loginView({ el: $("#mainContainer") });
                   view.render();
         },

 	});
 	 var ApplicationView = Backbone.View.extend({

         //bind view to body element (all views should be bound to DOM elements)
         el: $('body'),

         //observe navigation click events and map to contained methods
         events: {
             'click #home': 'displayHome',
             'click #register': 'displayRegistration',
             'click #login':'displayLogin'

         },

         //called on instantiation
         initialize: function(){
             //set dependency on ApplicationRouter
             this.router = new ApplicationRouter();
             landingPage.renderDefault();
             //call to begin monitoring uri and route changes
             Backbone.history.start();
         },

         displayHome: function(){
             //update url and pass true to execute route method
             this.router.navigate("home", true);
         },
         displayRegistration: function(){
        	  this.router.navigate("register", true);
         },
         displayLogin: function(){
             //update url and pass true to execute route method
             this.router.navigate("login", true);
         }
/*
         displayAbout: function(){
             //update url and pass true to execute route method
             this.router.navigate("about", true);
         },

         displayContact: function(){
             //update url and pass true to execute route method
             this.router.navigate("contact", true);
         }
*/
     });

     //load application
     new ApplicationView();

});