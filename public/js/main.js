require.config({
 baseUrl:'js',
 paths:{
	 "app":"app",
	 "text":"../lib/require/text",
	 "AdminModality":"../modalities/AdminModality",
	 "chefForms":"../views/chefForms",
     "ItemModality":"../views/ItemModality",
	 "ChefModality":"../modalities/ChefModality",
	 "loginView":"../views/loginView",
	 "registerView":"../views/registerView",
	 "CustomerModality":"../modalities/CustomerModality",
	 "LandingPage":"../modalities/LandingPage"
 }
});