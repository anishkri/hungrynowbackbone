define('LandingPage',['text!../html/landingPage.html','ChefModality','CustomerModality','AdminModality','registerView','loginView','menuView','MenuModel','MenuCollection'],function(masterViewTemp,ChefModality,CustomerModality,AdminModality,registerView,loginView,menuView,MenuModel,MenuCollection){
	function LandingPage(){
		this.LandingPage = Backbone.Model.extend({
			defaults:{

			},
			initialize : function(){

			},
		});
		this.LandingView = Backbone.View.extend({
			tagname:'div',
			template : _.template(masterViewTemp),
			events : {
				'click #todaysSpecial' : 'openMenu'
				/*"click #admin":'openadmin',*/
			/*	"click #register":'showRegister'*/
			},
			openChef : function(){
				/*var chef = new ChefModality({ el: $("#mainContainer") });
				chef.renderDefault();*/
				
				console.log("svygsdv");
			},
			openCust : function(){
				
				console.log("customer");
			},
            openMenu : function(){
                var menuCollection = new MenuCollection, menu = new menuView;
                menu.render(menuCollection);
                var foodQuery = new Parse.Query("FoodItem");
                foodQuery.greaterThan("price",150);
                foodQuery.include("user");
                foodQuery.find({
                    success:function(items){
                        items.forEach(function(item){
                            var menuModel = new MenuModel;
                            menuModel.set({name:item.get("name"), objectId:item.id, description:item.get("description"), price:item.get("price"), quantity_remaining:item.get("quantity_remaining"), imageURL:item.get("cover_image").url(), chef_name:item.get("user").get("screen_name"),"quantity_ordered":0});
                            menuCollection.add(menuModel);
                        });
                    },
                    error: function(error){
                        alert(error.code+" "+error.message);
                    }
                });
            },
		    /*openAdmin : function(){
				var admin = new AdminModality({ el: $("#mainContainer") });
				admin.renderDefault();
			},*/
			initialize:function(options){
				console.log('initialize view');
			},
			render:function(data,el){
			/*	var s = $(this.template(data)).html();*/
				var s =  _.template( $(this.template(data)).html(), {} );
				this.$el.html( s );
			}
		});
		this.init();
	}
	LandingPage.prototype.init = function(data){
		this.landingPage = new this.LandingPage(data);
		this.landingView = new this.LandingView({ el: $("#mainContainer") },data);
			/*	provide suitable ID everytime to avoid error*/

	};
	LandingPage.prototype.renderDefault = function(data){
		this.landingView.render(this.landingPage.attributes,function(){
        });
	};
	return LandingPage;
});