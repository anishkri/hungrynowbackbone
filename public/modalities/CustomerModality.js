define('CustomerModality',['text!../html/customer.html','orderView','OrderItemCollection','OrderItemModel','OrderModel','orderItemView'],function(masterViewTemp,orderView,OrderItemCollection,OrderItemModel,OrderModel,orderItemView){
	function CustomerModality(){
		 this.CustomerModel = Backbone.Model.extend({
			  defaults: {
			    name: 'Vipin',
			    age: '24',
			    
			  },
			  initialize: function(){
               console.log('This customer has been initialized.');
           /*    this.on('change', function(){
                   console.log('- Values for this model have changed.');
               });*/
             },
             render : function(){
                
             },
			});
		 this.CustomerView = Backbone.View.extend({
				tagname:'div',
				template : _.template(masterViewTemp),
				events:{
					"click #logOut":'logOut',
                    "click #orders": 'openOrders'
				},
				initialize : function(){
					console.log("Customer Intialized");
				},
                logOut : function(e){
                    e.preventDefault();
                    Parse.User.logOut();
                    alert("You have been logged out");
                    this.destroy_view();
                    location.reload();
                },
                destroy_view: function() {
                    this.undelegateEvents();
                    $(this.el).removeData().unbind(); 
                    this.remove();  
                    Backbone.View.prototype.remove.call(this);
                },
				render : function(user,el){
                    var userData = {"name": user.get("screen_name")};
                    var s = _.template($(this.template(userData)).html(),{});
                    $(el).html(s);
                    if(user.get("emailVerified")==false)
                        alert("Your email hasn't been verified yet");
				},
			    openOrders : function(){
                    var orderItemCollection = new OrderItemCollection({models: OrderItemModel}), orders = new orderView({collection:orderItemCollection});
                    var query = new Parse.Query(Objects.Order);
                    query.equalTo("objectId","qFtoSgyA3T");
                    query.include("item_detail");
                    query.include("item_detail.item");
                    query.include("address");
                    query.include("address.area");
                    query.include("address.area.city");
                    query.include("address.area.country");
                    query.include("address.area.state");
                    query.first({
                        success:function(order){
                            var orderModel = new OrderModel;
                            orderModel.setOrder(order);
                            var orderView = new orderItemView({model: orderModel});
                            orderView.render();
                        },
                        error:function(){
                        }
                    });
                }
		 });
	this.init();
	}
	CustomerModality.prototype.init = function(data){
		this.chefModel = new this.CustomerModel(data);
		this.masterView = new this.CustomerView({ el: $("#mainContainer") },data);
			/*	provide suitable ID everytime to avoid error*/

	};
	CustomerModality.prototype.renderDefault = function(data,el){
		this.masterView.render(data,el);
	};
	return CustomerModality;
});