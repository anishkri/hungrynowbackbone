define('OrderItemCollection',['OrderItemModel'],function(OrderItemModel){
	"use strict";
    var OrderItemCollection = Backbone.Collection.extend({
        models: OrderItemModel
    });
    return OrderItemCollection;
});