define('ItemCollection',['ItemModel'],function(ItemModel){
	"use strict";
    var ItemCollection = Backbone.Collection.extend({
        models: ItemModel
    });
    return ItemCollection;
});