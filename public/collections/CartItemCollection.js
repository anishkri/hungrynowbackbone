define('CartItemCollection',['CartItemModel'],function(CartItemModel){
	"use strict";
    var CartItemCollection = Backbone.Collection.extend({
        models: CartItemModel
    });
    return CartItemCollection;
});