define('MenuCollection',['MenuModel'],function(MenuModel){
	"use strict";
    var MenuCollection = Backbone.Collection.extend({
        models: MenuModel
    });
    return MenuCollection;
});