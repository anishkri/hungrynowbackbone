define('OrderItemModel',function(){
    var OrderItemModel = Backbone.Model.extend({
        defaults: {
            "name": "", 
            "objectId": "",
            "quantity": "",
            "price": ""
        },
        initialize: function(){
            
        },
        setItem: function(item){
            this.set("name",item.get("item").get("name"));
            this.set("objectId",item.id);
            this.set("quantity",item.get("item").get("quantity"));
            this.set("price",item.get("item").get("price"));
        },
        deleteItem: function(){
            this.destroy();
        }
    });
    return OrderItemModel;
});