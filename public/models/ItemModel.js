define('ItemModel',function(){
    var ItemModel = Backbone.Model.extend({
        defaults: {
            "name": "", 
            "objectId": "",
            "description": "",
            "price": "",
            "quantity": "",
            "imageURL": "",
            "available_on": "",
            "available_for": ""
        },
        deleteItem:function(){
            this.destroy();
        },
        initialize: function(){

        }
    });
    return ItemModel;
});