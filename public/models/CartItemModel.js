define('CartItemModel',[],function(){
    var CartItemModel = Backbone.Model.extend({
        defaults: {
            "name": "", 
            "objectId": "",
            "quantity_ordered": "",
            "price": "",
            "total_price": ""
        },
        initialize: function(){
            
        },
        setItem: function(item){
            this.set({
        		"name":item.get("name"),
        		"price":item.get("price"),
        		"objectId":item.get("objectId"),
        		"quantity_ordered":item.get("quantity_ordered"),
                "total_price":item.get("total_price")
        	});
        },
        deleteItem: function(){
            this.destroy();
        }
    });
    return CartItemModel;
});