define('MenuModel',function(){
	var MenuModel = Backbone.Model.extend({
		 defaults: {
            "name": "", 
            "objectId": "",
            "description": "",
            "price": "",
            "imageURL": "",
            "quantity_remaining": "",
            "total_price": ""
        },
        initialize : function(){
        	console.log("Menu-model initialize ")
        }
	});
	return MenuModel;
	
});