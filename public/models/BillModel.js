define('BillModel',[],function(){
    var BillModel = Backbone.Model.extend({
        defaults: {
            "name": "", 
            "objectId": "",
            "quantity_ordered": "",
            "price": "",
            "total_price": ""
        },
        initialize: function(){
            
        },
        setItem: function(item,total){
            this.set({
        		"name":item.get("name"),
        		"price":item.get("price"),
        		"objectId":item.get("objectId"),
        		"quantity_ordered":item.get("quantity_ordered"),
                "total_price":total,
                
        	});
        },
        deleteItem: function(){
            this.destroy();
        }
    });
    return BillModel;
});