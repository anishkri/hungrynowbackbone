define('OrderModel',function(){
	var OrderModel = Backbone.Model.extend({
		 defaults: {
            "objectId": "",
            "total_price": "190"
        },
        initialize : function(){
        	console.log("Order-model initialize ")
        },
        setOrder: function(order){
            var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
            var orderDate = order.createdAt.getDate()+" "+months[order.createdAt.getMonth()]+" "+order.createdAt.getFullYear();
            var hours = order.createdAt.getHours(), minutes = order.createdAt.getMinutes(), ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var orderTime = hours+"."+minutes+" "+ampm;
            var items = order.get("item_detail"), itemNames = [];
            items.forEach(function(item){
                itemNames.push(item.get("item").get("name")+" x "+item.get("quantity")+" ,");
            });
            itemNames[itemNames.length-1] = itemNames[itemNames.length-1].slice(0,-1);
            this.set("objectId",order.id);
            this.set("items",itemNames);
            this.set("createdAt",orderDate+" "+orderTime);
            this.set("name",order.get("email"));
            this.set("contact_no",order.get("contact_no").substr(2));
            this.set("address_line_1",order.get("address").get("address_line_1"));
            this.set("address_line_2",order.get("address").get("address_line_2"));
            this.set("area_name",order.get("address").get("area").get("name"));
            this.set("pin_code",order.get("address").get("area").get("pin_code"));
            this.set("city_name",order.get("address").get("area").get("city").get("name"));
            this.set("state_name",order.get("address").get("area").get("state").get("name"));
        },
        deleteItem: function(){
            this.destroy();
        }
	});
	return OrderModel;
	
});