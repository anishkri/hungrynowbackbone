var express = require('express');
var HungryNow = require('cloud/objects/objects.js');

exports.home = function(req, res, next) {
    if (Parse.User.current()) {
        var query = new Parse.Query(Parse.User);
        query.equalTo("objectId", Parse.User.current().id);
        query.equalTo("type", "chef");
        query.equalTo("isChefVerified", true);
        query.find({
            success: function(results) {
                if (results.length <= 0) {
                    res.redirect("/")
                } else {
                    var foodQuery = new HungryNow.Objects.FoodItem();
                    foodQuery.equalTo("user", Parse.User.current());
                    foodQuery.find({
                        success: function(results) {
                            var FoodItemCollection = Parse.Collection.extend(HungryNow.Objects.FoodItem);
                            var collection = new FoodItemCollection();
                            collection.add(results);
                            console.log({
                                "results": collection.toJSON()
                            });
                            res.render("chef/chefHome", {
                                "results": collection.toJSON()
                            });
                        }
                    });
                }
            },
            error: function() {
                res.error("No User With Object Id");
            }
        });
    } else {
        res.redirect('/login');
    }
}


exports.addMeal = function(req, res, next) {
    if (Parse.User.current()) {
        var query = new Parse.Query(Parse.User);
        query.equalTo("objectId", Parse.User.current().id);
        query.equalTo("type", "chef");
        query.equalTo("isChefVerified", true);
        query.find({
            success: function(results) {
                if (results.length <= 0) {
                    res.redirect("/")
                } else {
                    var foodQuery = new Parse.Query("FoodItem");
                    foodQuery.equalTo("user", Parse.User.current());
                    foodQuery.find({
                        success: function(results) {
                            res.render("chef/addAMeal");
                        }
                    });
                }
            },
            error: function() {
                res.error("No User With Object Id");
            }
        });
    } else {
        res.redirect('/login');
    }
}