var express = require('express');
var HungryNow = require('cloud/objects/objects.js');


exports.home = function(req, res, next) {
    var query = new Parse.Query(HungryNow.Objects.Order);
    query.greaterThan("createdAt", new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0, 0));
    query.include("address");
    query.include("address.area");
    query.include("address.area.city");
    query.include("address.area.state");
    query.include("address.area.country");
    query.include("item_detail");
    query.include("item_detail.item");
    query.ascending("order_status");
    query.find({
        success: function(results) {
            var renderArray = new Array();
            results.forEach(function(result) {
                var itemDetails = result.get('item_detail');
                var items = new Array();
                
                if (itemDetails) {
                    itemDetails.forEach(function(item) {
                        items.push({
                            'name': item.get("item").get('name'),
                            'quantity': item.get('quantity'),
                        });

                    });
                }
                result.set("item_list", items);
                var city = result.get("address").get("area").get("city");
                result.set("city", city.toJSON());
                var state = result.get("address").get("area").get("state");
                result.set("state", state.toJSON());
                var country = result.get("address").get("area").get("country");
                result.set("country", country.toJSON());
                var area = result.get("address").get("area");
                result.set("area", area.toJSON());
                var address = result.get("address");
                result.set("address", address.toJSON());
                renderArray.push(result.toJSON());
            });
            res.render("delivery/delivery_home.ejs", {
                "results": renderArray
            });
        },
        error: function() {
            res.error("Orders could not be retrieved");
        }
    });
}