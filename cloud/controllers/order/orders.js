var express = require('express');
var ejs = require('ejs');
var HungryNow = require('cloud/objects/objects.js');
var Mandrill = require('mandrill');
Mandrill.initialize('fYD2Dqwg9DH5lr_QaE3DDg');


exports.orderNow = function(req, res) {
    var order = new HungryNow.Objects.Order();
    var address = new HungryNow.Objects.Address();
    address.id = req.body.address;
    var orderObject = JSON.parse(req.body.orderDetails);
    var keyArray = [];
    Object.keys(orderObject).forEach(function(key) {
        keyArray.push(key);
    });
    Parse.Cloud.useMasterKey();

    var foodItemQuery = new Parse.Query(HungryNow.Objects.FoodItem);
    foodItemQuery.containedIn('objectId', keyArray);
    foodItemQuery.find({
        success: function(results) {
            var isOrderValid = true;
            var faultyOrders = {};
            var items = [];
            var itemsToRender = [];
            var itemDetails = [];
            for (var i = 0; i < results.length; i++) {
                console.log(results[i].get('quantity_remaining'));
                console.log("The Object  Is " + orderObject[results[i].id]);

                console.log(parseInt(results[i].get('quantity_remaining')) - parseInt(orderObject[results[i].id]));

                if (parseInt(orderObject[results[i].id]) > parseInt(results[i].get('quantity_remaining'))) {
                    isOrderValid = false;
                    faultyOrders[results[i].id] = results[i];
                } else {
                    results[i].set("quantity_remaining", parseInt(results[i].get('quantity_remaining')) - parseInt(orderObject[results[i].id]));
                    items.push({
                        item: results[i],
                        quantity: parseInt(orderObject[results[i].id])
                    });
                    itemsToRender.push({
                        item: results[i].toJSON(),
                        quantity: parseInt(orderObject[results[i].id])
                    });
                    var orderItemDetail = new HungryNow.Objects.OrderItemDetail();
                    orderItemDetail.set("item", results[i]);
                    orderItemDetail.set("quantity", parseInt(orderObject[results[i].id]));
                    itemDetails.push(orderItemDetail);
                }
            }

            if (!isOrderValid) {
                res.json(500, faultyOrders);
            } else {
                Parse.Object.saveAll(results, {
                    success: function(list) {
                        order.set("contact_no", req.body.phNo);
                        order.set("email", req.body.emailId);
                        order.set("address", address);
                        order.set('item_detail', itemDetails);
                        order.set('order_status', 0);
                        var content =
                            order.save(null, {
                                success: function(order) {
                                    console.log(items);
                                    ejs.renderFile('cloud/views/email/order_details.ejs', {
                                        "order": order.toJSON(),
                                        items: itemsToRender
                                    }, function(err, html) {
                                        if (err) {
                                            console.log("The Error Is " + err);
                                        }
                                        console.log("The Html Template File Is" + html);
                                        Mandrill.sendEmail({
                                            message: {
                                                subject: "Order Confirmed",
                                                html: html,
                                                from_email: "orders@hungrynow.in",
                                                from_name: "Orders@HungryNow",
                                                to: [{
                                                    email: req.body.emailId
                                                }]
                                            },
                                            async: true
                                        }, {
                                            success: function(httpResponse) {
                                                console.log(httpResponse);
                                                //response.success("Email sent!");
                                            },
                                            error: function(httpResponse) {
                                                console.error(httpResponse);
                                                //response.error("Uh oh, something went wrong");
                                            }
                                        });
                                        res.json(200, order);
                                    });


                                },
                                error: function(error) {
                                    res.json(500, error);
                                }
                            });
                    },
                    error: function(error) {
                        res.error("Order Counl Not Be Saved ");
                    },
                });

            }
        },
        error: function(error) {
            res.error(error);
        }
    });

}