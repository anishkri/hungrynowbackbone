// These two lines are required to initialize Express in Cloud Code.

/*var express = require('express');
var moment = require('moment');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
var parseExpressCookieSession = require('parse-express-cookie-session');
var HungryNow = require('cloud/objects/objects.js');
var chef = require("cloud/controllers/chef/chef.js");
var orders = require("cloud/controllers/order/orders.js");
var delivery = require("cloud/controllers/delivery/delivery.js");
var Mandrill = require('mandrill');*/



//Declare Variables
//var app = express();


//Init Variables
//Mandrill.initialize('fYD2Dqwg9DH5lr_QaE3DDg');



/*app.set('views', 'cloud/views');
app.set('view engine', 'ejs');
app.use(parseExpressHttpsRedirect()); // Require user to be on HTTPS.
app.use(express.bodyParser());
app.use(express.cookieParser('a6a82dd3541d'));
app.use(parseExpressCookieSession({
    cookie: {
        maxAge: 3600000
    }
}));*/

//Chef Routing
/*
app.get('/chef', chef.home);
app.get('/chef/addMeal', chef.addMeal);
*/


//Order Routing

//app.post('/orderNow', orders.orderNow);


//Delivery Routing
//app.get('/delivery', delivery.home);


app.get("/", function(req, res) {
    var foodQuery = new Parse.Query("FoodItem");
    var startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0, 0);
    console.log("Start Date Is " + startDate);
    var endDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 23, 59, 59, 0);
    console.log("End Date Is " + endDate);
    foodQuery.include("user");
    foodQuery.greaterThanOrEqualTo('available_on', startDate);
    foodQuery.lessThanOrEqualTo('available_on', endDate);
    foodQuery.equalTo('available_for', "1");
    foodQuery.descending("quantity_remaining");
    //foodQuery.greaterThan('quantity_remaining', 0);
    console.log("The Session Is + " + req.session);
    foodQuery.find({
        success: function(results) {
            /*var FoodItemCollection = Parse.Collection.extend({
                model: "FoodItem"
            });*/
            var collection = new Parse.Collection(results);
            collection.forEach(function(object) {
                var user = object.get("user");
                object.set("user", user.toJSON());
            });
            console.log({
                "results": collection.toJSON()
            });
            res.render("home.ejs", {
                "results": collection.toJSON()
            });
        },
        error: function() {
            res.error("No User With Object Id");
        }
    });
});


app.get('/login', function(req, res) {
    // Renders the login form asking for username and password.
    res.render('login.ejs');
});

// Clicking submit on the login form triggers this.
app.post('/login', function(req, res) {
    Parse.User.logIn(req.body.username, req.body.password).then(function() {
            // Login succeeded, redirect to homepage.
            // parseExpressCookieSession will automatically set cookie.
            res.redirect('/');
        },
        function(error) {
            // Login failed, redirect back to login form.
            res.redirect('/login');
        });
});

// You could have a "Log Out" link on your website pointing to this.
app.get('/logout', function(req, res) {
    Parse.User.logOut();
    res.redirect('/');
});

app.post('/register/chef/', function(req, res) {

    var user = new Parse.User();
    user.set("username", req.body.email);
    user.set("password", req.body.password);
    user.set("email", req.body.email);
    user.set("phone", req.body.phone);
    user.set("type", "chef");
    user.set("isChefVerified", false);
    user.set("screen_name", req.body.name);

    user.signUp(null, {
        success: function(user) {
            res.send("Registration Successful");
        },
        error: function(user, error) {
            // Show the error message somewhere and let the user try again.
            res.error(error.code);
        }
    });

});


app.post("/food_item/latestCount/", function(req, res) {
    console.log(req.body.food_item_ids[0]);
    console.log(req.body.food_item_ids[1]);
    var queryArray = JSON.parse(req.body.food_item_ids);
    console.log(queryArray[1]);
    var query = new Parse.Query("FoodItem");
    query.containedIn('objectId', queryArray);
    query.select('objectId');
    query.select('countRemaining');
    query.find({
        success: function(results) {
            res.json(results);
        },
        error: function(error) {
            res.error("Error: " + error.code + " " + error.message);
        }
    });
});

app.get('/currentUser', function(req, res) {
    var query = new Parse.Query(Parse.User);
    query.equalTo("objectId", Parse.User.current().id);
    query.find({
        success: function(results) {
            res.json(results[0]);
        },
        error: function() {
            res.error("No User With Object Id");
        }
    });

});


app.listen();