exports.Objects = {
    Order : Parse.Object.extend('Order'),
    Address : Parse.Object.extend("Address"),
    FoodItem : Parse.Object.extend("FoodItem"),
    OrderItemDetail : Parse.Object.extend("OrderItemDetail")
};